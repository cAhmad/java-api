package fr.ing.interview.javaapi.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    private UserService sut;

    @Before
    public void setUp() {
        sut = new UserService();
    }

    @Test
    public void should_add_user() {
        final User userToAdd = new User(4, "Ahmad", "ahmad@ing.com");
        final User userAdded = sut.add(userToAdd);
        assertThat(userAdded, is(userToAdd));

        final List<User> users = sut.getUsers();
        assertThat(users.size(), is(4));
    }

    @Test
    public void should_not_add_user() {
        final User userToAdd = new User(1, "Ahmad", "Ahmad@ing.com");
        final User userAdded = sut.add(userToAdd);
        assertThat(userAdded, is(userToAdd));

        final List<User> users = sut.getUsers();
        assertThat(users.size(), is(3));
    }

    @Test
    public void should_get_user_by_id() {
        final String name = "Bob";
        final String email = "bob@ing.com";
        final int id = 1;

        final User userFound = sut.getById(id);
        assertNotNull(userFound);
        assertThat(userFound.getId(), is(id));
        assertThat(userFound.getName(), is(name));
        assertThat(userFound.getEmail(), is(email));
    }

    @Test(expected = UserNotFoundException.class)
    public void should_not_found_user_by_id() {
        final int badId = 4;
        sut.getById(badId);
    }

    @Test
    public void should_return_3_users() {
        final List<User> users = sut.getUsers();
        assertThat(users.size(), is(3));
    }

    @Test
    public void should_update__user() {
        final User userToUpdate = new User(1, "Ahmad", "ahmad@ing.com");
        final User UserUpdated = sut.update(userToUpdate);
        assertThat(UserUpdated, is(userToUpdate));

        final List<User> users = sut.getUsers();
        assertThat(users.size(), is(3));
    }

    @Test(expected = UserNotFoundException.class)
    public void should_not_update_user() {
        final User userToAdd = new User(4, "Ahmad", "ahmad@ing.com");
        sut.update(userToAdd);
    }
}
