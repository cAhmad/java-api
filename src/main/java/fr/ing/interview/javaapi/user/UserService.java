package fr.ing.interview.javaapi.user;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {

    private final List<User> users;

    public UserService() {
        this.users = generateUsers();
    }


    User add(User user) {
        final int userIndex = users.indexOf(user);
        if (userIndex != -1) {
            return user;
        }
        users.add(user);
        return user;
    }

    User getById(int id) {
        return users.stream().filter(user -> user.getId() == id)
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    User update(User user) {
        final int userIndex = users.indexOf(user);
        if (userIndex == -1) {
            throw new UserNotFoundException(user);
        }

        return users.set(userIndex, user);
    }

    List<User> getUsers() {
        return users;
    }


    private List<User> generateUsers() {
        return new ArrayList<>(Arrays.asList(
                new User(1, "Bob", "bob@ing.com"),
                new User(2, "Alice", "alice@ing.com"),
                new User(3, "admin", "admin@ing.com")
        ));
    }

}
