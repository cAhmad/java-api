package fr.ing.interview.javaapi.user;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*
    Create a CRUD API with Spring MVC with associated tests
 */
@RestController(UserController.API_PATH)
public class UserController {

    final static String API_PATH = "/users";

    @GetMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    public String getWelcomeMessage() {
        return String.format("Hello %s API", API_PATH);
    }
}
