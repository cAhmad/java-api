package fr.ing.interview.javaapi.user;

class UserNotFoundException extends RuntimeException {

    private static final String MESSAGE_FOR_ID = "User with id: %d is not found";
    private static final String MESSAGE_FOR_OBJECT = "User: %s is not found";

    public UserNotFoundException(int id) {
        super(String.format(MESSAGE_FOR_ID, id));
    }

    UserNotFoundException(User user) {
        super(String.format(MESSAGE_FOR_OBJECT, user));
    }
}
